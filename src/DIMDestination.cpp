/*
 * DIMDestination.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: mdonze
 */

#include "DIMDestination.h"

#include <iostream>

#include "Constants.h"
#include "DIMClient.h"
#include "DIMXMLCommand.h"
#include "pugixml.hpp"

namespace ntof {
namespace proxy {

DIMDestination::DIMDestination(DIMClient *client) : client_(client) {}

DIMDestination::~DIMDestination()
{
    mq.reset();
}

void DIMDestination::start()
{
    std::string queueName;
    std::size_t maxMsg;
    std::size_t maxMsgSize;
    client_->getQueueConfiguration(queueName, maxMsg, maxMsgSize);
    cmdQueueName = queueName + client_->getSourceName() + "Cmd";
    mq.reset(new boost::interprocess::message_queue(
        boost::interprocess::open_or_create, cmdQueueName.c_str(), maxMsg,
        maxMsgSize));
    consumerThread = std::thread(&DIMDestination::consume, this);
}

void DIMDestination::consume()
{
    std::cout << "Waiting to consume DIM commands from " << cmdQueueName
              << std::endl;
    while (true)
    {
        boost::interprocess::message_queue::size_type recvd_size;
        try
        {
            unsigned int priority;
            std::string rcvString;
            rcvString.resize(mq->get_max_msg_size());
            mq->receive(&rcvString[0], mq->get_max_msg_size(), recvd_size,
                        priority);
            rcvString.resize(recvd_size);
            // Got some data
            pugi::xml_document doc;
            pugi::xml_parse_result parseResult = doc.load_buffer(
                rcvString.c_str(), rcvString.size());
            if (parseResult)
            {
                // XML parsing without errors
                std::string commandName =
                    doc.first_child().attribute("from").value();
                pugi::xml_document cmdDoc;
                cmdDoc.append_copy(doc.first_child().first_child());
                ntof::dim::DIMXMLCommand::sendCommandNB(commandName, cmdDoc);
            }
            else
            {
                std::cerr << "Error while processing XML command..."
                          << std::endl;
            }
        }
        catch (boost::interprocess::interprocess_exception &ex)
        {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void DIMDestination::registerNewCommand(std::string &from, std::string &to)
{
    pugi::xml_document doc;
    pugi::xml_node aqnNode = doc.append_child("command");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(from.c_str());
    aqnNode.append_attribute("to").set_value(to.c_str());
    client_->postData(doc);
}

} /* namespace proxy */
} /* namespace ntof */
