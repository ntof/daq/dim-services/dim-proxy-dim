/*
 * DIMDestination.h
 *
 *  Created on: Feb 9, 2015
 *      Author: mdonze
 */

#ifndef DIMDESTINATION_H_
#define DIMDESTINATION_H_

#include <memory>
#include <string>
#include <thread>

#include <boost/interprocess/ipc/message_queue.hpp>

namespace ntof {
namespace proxy {
class DIMClient;

class DIMDestination
{
public:
    explicit DIMDestination(DIMClient *client);
    virtual ~DIMDestination();
    void consume();
    void registerNewCommand(std::string &from, std::string &to);
    void start();

private:
    std::string cmdQueueName;
    std::unique_ptr<boost::interprocess::message_queue> mq;
    DIMClient *client_; //!<< Reference to client
    std::thread consumerThread;
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DIMDESTINATION_H_ */
