/*
 * DIMSource.h
 *  DIM data source
 *  Created on: Jan 15, 2015
 *      Author: mdonze
 */

#ifndef DIMSOURCE_H_
#define DIMSOURCE_H_

#include <string>

#include "DIMXMLInfo.h"
#include "DIMXMLService.h"

namespace ntof {
namespace proxy {
class DIMClient;

class DIMSource : public ntof::dim::DIMXMLInfoHandler
{
public:
    DIMSource(const std::string &dimSrc,
              const std::string &dimDest,
              DIMClient *client);
    virtual ~DIMSource();
    /**
     * Callback when an error is made by XML parser
     * @param errMsg Error message in string
     * @param info DIMXMLInfo object who made this callback
     */
    void errorReceived(std::string errMsg, const ntof::dim::DIMXMLInfo *info);

    /**
     * Callback when a new DIMXMLInfo is received
     * @param doc XML document containing new data
     * @param info DIMXMLInfo object who made this callback
     */
    void dataReceived(pugi::xml_document &doc,
                      const ntof::dim::DIMXMLInfo *info);

    /**
     * Callback when a not link is present on DIMXMLInfo
     * @param info DIMXMLInfo object who made this callback
     */
    void noLink(const ntof::dim::DIMXMLInfo *info);

private:
    std::string dimSvcName;       //!<< DIM service name
    std::string destName;         //!<< Destination name
    ntof::dim::DIMXMLInfo dimSub; //!<< DIM subscription object
    DIMClient *client_;           //!<< Reference to client
    pugi::xml_document doc;       //!<< This XML document

    DIMSource(const DIMSource &other);            //!<< Don't allow copy
    DIMSource &operator=(const DIMSource &other); //!<< Don't allow copy
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* DIMSOURCE_H_ */
