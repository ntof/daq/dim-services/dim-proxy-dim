/*
 * DIMSource.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */

#include "DIMSource.h"

#include <iostream>

#include "Constants.h"
#include "DIMClient.h"
#include "DIMData.h"
#include "Utils.h"
#include "pugixml.hpp"
namespace ntof {
namespace proxy {

DIMSource::DIMSource(const std::string &dipSrc,
                     const std::string &dimDest,
                     DIMClient *client) :
    dimSvcName(dipSrc), destName(dimDest), dimSub(dipSrc, this), client_(client)
{
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(dipSrc.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    client_->postData(doc);
}

DIMSource::~DIMSource() {}

// Various DIM handlers
void DIMSource::dataReceived(pugi::xml_document &docRecv,
                             const ntof::dim::DIMXMLInfo * /*info*/)
{
    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(dimSvcName.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    aqnNode.append_copy(docRecv.first_child());
    client_->postData(doc);
}

void DIMSource::noLink(const ntof::dim::DIMXMLInfo * /*info*/)
{
    std::cout << "No link on " << dimSvcName << " requesting service removal..."
              << std::endl;
    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("remove");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(dimSvcName.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    pugi::xml_node errNode = aqnNode.append_child("error");
    errNode.append_child(pugi::node_pcdata).set_value("NO LINK");
    client_->postData(doc);
}

void DIMSource::errorReceived(std::string errMsg,
                              const ntof::dim::DIMXMLInfo * /*info*/)
{
    std::cout << "DIMXMLInfo error on " << dimSvcName << " publishing error..."
              << std::endl;
    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(dimSvcName.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    pugi::xml_node errNode = aqnNode.append_child("error");
    errNode.append_child(pugi::node_pcdata).set_value(errMsg.c_str());
    client_->postData(doc);
}

} /* namespace proxy */
} /* namespace ntof */
