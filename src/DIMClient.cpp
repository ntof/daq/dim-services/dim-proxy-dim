/*
 * DIMClient.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */
#include "DIMClient.h"

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include <boost/interprocess/ipc/message_queue.hpp>

#include "Constants.h"
#include "DIMSource.h"

#include <dic.hxx>

namespace ntof {
namespace proxy {

DIMClient::DIMClient() :
    sourceName("DIM"),
    maxMsg(MAX_MSG),
    maxMsgSize(MAX_MSG_SIZE),
    queueName(MQ_NAME)
{}

DIMClient::~DIMClient()
{
    mq.reset();
}

void DIMClient::postData(pugi::xml_document &doc)
{
    std::ostringstream oss;
    // Uncomment end of line for raw XML output
    doc.save(oss); //,"\t", pugi::format_raw);
    try
    {
        mq->try_send(oss.str().c_str(), oss.str().size(), 0);
    }
    catch (...)
    {
        std::cout << "Exception while posting data into queue..." << std::endl;
    }
}

bool DIMClient::loadConfig(std::string cfgPath)
{
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load_file(cfgPath.c_str());
    if (res)
    {
        std::string dnsNode = "pcgw25";
        pugi::xml_node root = doc.root().child("proxy");
        pugi::xml_node cfg = root.child("configuration");
        if (cfg)
        {
            if (cfg.child("dimDNSNode").attribute("name"))
            {
                dnsNode = cfg.child("dimDNSNode")
                              .attribute("name")
                              .as_string(dnsNode.c_str());
            }
            if (cfg.child("sourceName").attribute("name"))
            {
                sourceName = cfg.child("sourceName")
                                 .attribute("name")
                                 .as_string(sourceName.c_str());
                std::cout << "Source name : " << sourceName << std::endl;
            }

            pugi::xml_node queueCfg = cfg.child("queue");
            if (queueCfg)
            {
                queueName = queueCfg.attribute("name").as_string(MQ_NAME);
                maxMsg = queueCfg.attribute("count").as_int(MAX_MSG);
                maxMsgSize = queueCfg.attribute("size").as_int(MAX_MSG_SIZE);
            }
        }

        // Set properties from config
        DimClient::setDnsNode(dnsNode.c_str());

        mq.reset();
        while (!mq)
        {
            try
            {
                mq.reset(new boost::interprocess::message_queue(
                    boost::interprocess::open_only, queueName.c_str()));
            }
            catch (...)
            {
                std::cerr << "Waiting for message-queue" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(3));
            }
        }

        pugi::xml_node sourcesNode = root.child("sources");
        if (sourcesNode)
        {
            for (pugi::xml_node source = sourcesNode.first_child(); source;
                 source = source.next_sibling())
            {
                if (source.attribute("service") &&
                    source.attribute("destination"))
                {
                    sources.push_back(new ntof::proxy::DIMSource(
                        source.attribute("service").as_string(""),
                        source.attribute("destination").as_string(""), this));
                }
                else
                {
                    std::cerr << "Bad DIM service description." << std::endl;
                    std::cerr << "Expected service and destination attributes"
                              << std::endl;
                }
            }
        }

        dest.reset(new DIMDestination(this));

        pugi::xml_node destinationNodes = root.child("destinations");
        if (destinationNodes)
        {
            for (pugi::xml_node destNode = destinationNodes.first_child();
                 destNode; destNode = destNode.next_sibling())
            {
                if (destNode.attribute("source") &&
                    destNode.attribute("destination"))
                {
                    std::string fromValue = destNode.attribute("source").value();
                    std::string toValue =
                        destNode.attribute("destination").value();
                    dest->registerNewCommand(fromValue, toValue);
                }
                else
                {
                    std::cerr << "Bad DIM destination description."
                              << std::endl;
                    std::cerr << "Expected source and destination attributes"
                              << std::endl;
                }
            }
        }
        dest->start();
        return true;
    }
    else
    {
        return false;
    }
}

void DIMClient::heartBeat()
{
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(HEARTBEAT_RATE));
        pugi::xml_document hbDoc;
        pugi::xml_node hbNode = hbDoc.append_child("heartbeat");
        hbNode.append_attribute("source").set_value(sourceName.c_str());
        postData(hbDoc);
    }
}

/**
 * Gets the message queue configuration
 * @param name
 * @param msgMax
 * @param msgMaxSize
 */
void DIMClient::getQueueConfiguration(std::string &name,
                                      std::size_t &msgMax,
                                      std::size_t &msgMaxSize)
{
    name = queueName;
    msgMax = maxMsg;
    msgMaxSize = maxMsgSize;
}

} /* namespace proxy */
} /* namespace ntof */
